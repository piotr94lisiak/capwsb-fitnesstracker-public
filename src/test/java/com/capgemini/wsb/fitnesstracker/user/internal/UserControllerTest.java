package com.capgemini.wsb.fitnesstracker.user.internal;

import com.capgemini.wsb.fitnesstracker.user.api.User;
import com.capgemini.wsb.fitnesstracker.user.api.UserDto;
import com.capgemini.wsb.fitnesstracker.user.api.UserSimpleDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserControllerTest {

    @Mock
    private UserServiceImpl userService;

    @Mock
    private UserMapper userMapper;

    @InjectMocks
    private UserController userController;

    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @Test
    void shouldReturnAllSimpleUsers() throws Exception {
        User user1 = new User("John", "Doe", LocalDate.of(1990, 1, 1), "john.doe@example.com");
        User user2 = new User("Jane", "Smith", LocalDate.of(1992, 2, 2), "jane.smith@example.com");

        List<User> users = Arrays.asList(user1, user2);
        when(userService.findAllUsers()).thenReturn(users);
        when(userMapper.toSimpleDto(user1)).thenReturn(new UserSimpleDto(user1.getId(), user1.getFirstName(), user1.getLastName()));
        when(userMapper.toSimpleDto(user2)).thenReturn(new UserSimpleDto(user2.getId(), user2.getFirstName(), user2.getLastName()));

        mockMvc.perform(get("/v1/users/simple")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].firstName").value("John"))
                .andExpect(jsonPath("$[0].lastName").value("Doe"))
                .andExpect(jsonPath("$[1].firstName").value("Jane"))
                .andExpect(jsonPath("$[1].lastName").value("Smith"));
    }

    @Test
    void shouldReturnUserDetails_whenGettingUserById() throws Exception {
        User user = new User("John", "Doe", LocalDate.of(1990, 1, 1), "john.doe@example.com");
        UserDto userDto = new UserDto(1L, "John", "Doe", LocalDate.of(1990, 1, 1), "john.doe@example.com");

        when(userService.getUser(anyLong())).thenReturn(Optional.of(user));
        when(userMapper.toDto(user)).thenReturn(userDto);

        mockMvc.perform(get("/v1/users/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("John"))
                .andExpect(jsonPath("$.lastName").value("Doe"))
                .andExpect(jsonPath("$.birthdate").value("1990-01-01"))
                .andExpect(jsonPath("$.email").value("john.doe@example.com"));
    }

    @Test
    void shouldCreateUser() throws Exception {
        UserDto userDto = new UserDto(null, "John", "Doe", LocalDate.of(1990, 1, 1), "john.doe@example.com");
        User user = new User("John", "Doe", LocalDate.of(1990, 1, 1), "john.doe@example.com");
        User savedUser = new User("John", "Doe", LocalDate.of(1990, 1, 1), "john.doe@example.com");
        savedUser.setId(1L);

        when(userMapper.toEntity(userDto)).thenReturn(user);
        when(userService.createUser(user)).thenReturn(savedUser);
        when(userMapper.toDto(savedUser)).thenReturn(new UserDto(1L, "John", "Doe", LocalDate.of(1990, 1, 1), "john.doe@example.com"));

        mockMvc.perform(post("/v1/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                {
                                    "firstName": "John",
                                    "lastName": "Doe",
                                    "birthdate": "1990-01-01",
                                    "email": "john.doe@example.com"
                                }
                                """))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.firstName").value("John"))
                .andExpect(jsonPath("$.lastName").value("Doe"))
                .andExpect(jsonPath("$.birthdate").value("1990-01-01"))
                .andExpect(jsonPath("$.email").value("john.doe@example.com"));
    }

    @Test
    void shouldUpdateUser() throws Exception {
        UserDto userDto = new UserDto(null, "John", "Doe", LocalDate.of(1990, 1, 1), "john.doe@example.com");
        User user = new User("John", "Doe", LocalDate.of(1990, 1, 1), "john.doe@example.com");
        user.setId(1L);
        User updatedUser = new User("John", "Doe", LocalDate.of(1990, 1, 1), "john.doe@example.com");
        updatedUser.setId(1L);

        when(userMapper.toEntity(userDto)).thenReturn(user);
        when(userService.updateUser(user)).thenReturn(updatedUser);
        when(userMapper.toDto(updatedUser)).thenReturn(new UserDto(1L, "John", "Doe", LocalDate.of(1990, 1, 1), "john.doe@example.com"));

        mockMvc.perform(put("/v1/users/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                {
                                    "firstName": "John",
                                    "lastName": "Doe",
                                    "birthdate": "1990-01-01",
                                    "email": "john.doe@example.com"
                                }
                                """))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("John"))
                .andExpect(jsonPath("$.lastName").value("Doe"))
                .andExpect(jsonPath("$.birthdate").value("1990-01-01"))
                .andExpect(jsonPath("$.email").value("john.doe@example.com"));
    }

    @Test
    void shouldDeleteUser() throws Exception {
        mockMvc.perform(delete("/v1/users/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }
}
