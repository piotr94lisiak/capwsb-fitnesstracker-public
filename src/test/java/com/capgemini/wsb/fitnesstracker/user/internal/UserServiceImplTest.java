package com.capgemini.wsb.fitnesstracker.user.internal;

import com.capgemini.wsb.fitnesstracker.user.api.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testCreateUser() {
        User user = new User("John", "Doe", LocalDate.of(1990, 1, 1), "john.doe@example.com");
        when(userRepository.save(any(User.class))).thenReturn(user);

        User createdUser = userService.createUser(user);

        assertNotNull(createdUser);
        assertEquals("John", createdUser.getFirstName());
        verify(userRepository, times(1)).save(user);
    }

    @Test
    void testDeleteUser() {
        Long userId = 1L;
        doNothing().when(userRepository).deleteById(userId);

        userService.deleteUser(userId);

        verify(userRepository, times(1)).deleteById(userId);
    }

    @Test
    void testUpdateUser() {
        User user = new User("John", "Doe", LocalDate.of(1990, 1, 1), "john.doe@example.com");
        when(userRepository.save(any(User.class))).thenReturn(user);

        User updatedUser = userService.updateUser(user);

        assertNotNull(updatedUser);
        assertEquals("John", updatedUser.getFirstName());
        verify(userRepository, times(1)).save(user);
    }

    @Test
    void testSearchUsersByEmail() {
        String email = "john.doe@example.com";
        User user = new User("John", "Doe", LocalDate.of(1990, 1, 1), email);
        when(userRepository.findByEmailContainingIgnoreCase(email)).thenReturn(Arrays.asList(user));

        List<User> users = userService.searchUsersByEmail(email);

        assertFalse(users.isEmpty());
        assertEquals(email, users.get(0).getEmail());
    }

    @Test
    void testFindUsersOlderThan() {
        int age = 30;
        User user = new User("John", "Doe", LocalDate.of(1990, 1, 1), "john.doe@example.com");
        when(userRepository.findByBirthdateBefore(any(LocalDate.class))).thenReturn(Arrays.asList(user));

        List<User> users = userService.findUsersOlderThan(age);

        assertFalse(users.isEmpty());
        assertTrue(users.get(0).getBirthdate().isBefore(LocalDate.now().minusYears(age)));
    }

    @Test
    void testGetUser() {
        Long userId = 1L;
        User user = new User("John", "Doe", LocalDate.of(1990, 1, 1), "john.doe@example.com");
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        Optional<User> foundUser = userService.getUser(userId);

        assertTrue(foundUser.isPresent());
        assertEquals("John", foundUser.get().getFirstName());
    }

    @Test
    void testGetUserByEmail() {
        String email = "john.doe@example.com";
        User user = new User("John", "Doe", LocalDate.of(1990, 1, 1), email);
        when(userRepository.findByEmail(email)).thenReturn(Optional.of(user));

        Optional<User> foundUser = userService.getUserByEmail(email);

        assertTrue(foundUser.isPresent());
        assertEquals(email, foundUser.get().getEmail());
    }

    @Test
    void testFindAllUsers() {
        User user1 = new User("John", "Doe", LocalDate.of(1990, 1, 1), "john.doe@example.com");
        User user2 = new User("Jane", "Doe", LocalDate.of(1995, 5, 5), "jane.doe@example.com");
        when(userRepository.findAll()).thenReturn(Arrays.asList(user1, user2));

        List<User> users = userService.findAllUsers();

        assertFalse(users.isEmpty());
        assertEquals(2, users.size());
    }

    @Test
    void shouldThrowExceptionWhenCreatingUserWithId() {
        User user = new User("John", "Doe", LocalDate.of(1990, 1, 1), "john.doe@example.com");
        user.setId(1L);

        assertThrows(IllegalArgumentException.class, () -> userService.createUser(user));
    }
}
