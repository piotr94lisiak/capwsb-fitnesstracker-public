package com.capgemini.wsb.fitnesstracker.scheduler;

import com.capgemini.wsb.fitnesstracker.mail.api.EmailDto;
import com.capgemini.wsb.fitnesstracker.mail.api.EmailSender;
import com.capgemini.wsb.fitnesstracker.training.api.Training;
import com.capgemini.wsb.fitnesstracker.training.internal.ActivityType;
import com.capgemini.wsb.fitnesstracker.training.internal.TrainingRepository;
import com.capgemini.wsb.fitnesstracker.user.api.User;
import com.capgemini.wsb.fitnesstracker.user.internal.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class WeeklyReportSchedulerTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private TrainingRepository trainingRepository;

    @Mock
    private EmailSender emailSender;

    @InjectMocks
    private WeeklyReportScheduler weeklyReportScheduler;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void shouldSendWeeklyReport() {
        // Mock data
        User user = new User("John", "Doe", LocalDate.of(1980, 1, 1), "john.doe@example.com");
        user.setId(1L);

        LocalDateTime now = LocalDateTime.now();
        Date startDate = Date.from(now.minusDays(2).atZone(ZoneId.systemDefault()).toInstant());
        Date endDate = Date.from(now.minusDays(1).atZone(ZoneId.systemDefault()).toInstant());

        Training training = new Training(user, startDate, endDate, ActivityType.RUNNING, 5.0, 10.0);
        training.setId(1L);

        when(userRepository.findAll()).thenReturn(Collections.singletonList(user));
        when(trainingRepository.findByUserId(anyLong())).thenReturn(Collections.singletonList(training));

        weeklyReportScheduler.generateAndSendWeeklyReports();

        verify(emailSender, times(1)).send(any(EmailDto.class));
    }
}
