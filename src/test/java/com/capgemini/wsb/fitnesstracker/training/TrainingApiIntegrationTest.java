package com.capgemini.wsb.fitnesstracker.training;

import com.capgemini.wsb.fitnesstracker.IntegrationTest;
import com.capgemini.wsb.fitnesstracker.IntegrationTestBase;
import com.capgemini.wsb.fitnesstracker.training.api.Training;
import com.capgemini.wsb.fitnesstracker.training.internal.ActivityType;
import com.capgemini.wsb.fitnesstracker.user.api.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static java.time.LocalDate.now;
import static java.util.UUID.randomUUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@IntegrationTest
@Transactional
@AutoConfigureMockMvc(addFilters = false)
class TrainingApiIntegrationTest extends IntegrationTestBase {

    @Autowired
    private MockMvc mockMvc;

    private SimpleDateFormat getUtcDateFormat() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf;
    }

    @Test
    void shouldReturnAllTrainings_whenGettingAllTrainings() throws Exception {
        User user1 = existingUser(generateClient(1L));
        Training training1 = persistTraining(generateTraining(user1));
        SimpleDateFormat sdf = getUtcDateFormat();
        mockMvc.perform(get("/v1/trainings").contentType(MediaType.APPLICATION_JSON))
                .andDo(log())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].user.id").value(user1.getId()))
                .andExpect(jsonPath("$[0].user.firstName").value(user1.getFirstName()))
                .andExpect(jsonPath("$[0].user.lastName").value(user1.getLastName()))
                .andExpect(jsonPath("$[0].user.email").value(user1.getEmail()))
                .andExpect(jsonPath("$[0].startTime").value(sdf.format(training1.getStartTime())))
                .andExpect(jsonPath("$[0].endTime").value(sdf.format(training1.getEndTime())))
                .andExpect(jsonPath("$[0].distance").value((training1.getDistance())))
                .andExpect(jsonPath("$[0].averageSpeed").value(training1.getAverageSpeed()))
                .andExpect(jsonPath("$[1]").doesNotExist());
    }

    @Test
    void shouldReturnAllFinishedTrainingsAfterTime_whenGettingAllFinishedTrainingsAfterTime() throws Exception {
        User user1 = existingUser(generateClient(1L));
        Training training1 = persistTraining(generateTrainingWithDetails(user1, "2024-05-19T17:00:00.000Z", "2024-05-19T18:30:00.000Z", ActivityType.RUNNING, 14, 11.5));
        Training training2 = persistTraining(generateTrainingWithDetails(user1, "2024-05-17T17:00:00.000Z", "2024-05-17T18:30:00.000Z", ActivityType.RUNNING, 14, 11.5));

        SimpleDateFormat sdf = getUtcDateFormat();
        mockMvc.perform(get("/v1/trainings/finished/{afterTime}", "2024-05-18T00:00:00.000Z").contentType(MediaType.APPLICATION_JSON))
                .andDo(log())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].user.id").value(user1.getId()))
                .andExpect(jsonPath("$[0].user.firstName").value(user1.getFirstName()))
                .andExpect(jsonPath("$[0].user.lastName").value(user1.getLastName()))
                .andExpect(jsonPath("$[0].user.email").value(user1.getEmail()))
                .andExpect(jsonPath("$[0].startTime").value(sdf.format(training1.getStartTime())))
                .andExpect(jsonPath("$[0].endTime").value(sdf.format(training1.getEndTime())))
                .andExpect(jsonPath("$[0].distance").value((training1.getDistance())))
                .andExpect(jsonPath("$[0].averageSpeed").value(training1.getAverageSpeed()))
                .andExpect(jsonPath("$[1]").doesNotExist());
    }

    private static User generateClient(Long id) {
        User user = new User("John", "Doe", now(), "john.doe@example.com");
        user.setId(id);
        return user;
    }


    private static Training generateTraining(User user) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        return new Training(
                user,
                sdf.parse("2024-01-19T07:00:00.000Z"),
                sdf.parse("2024-01-19T09:30:00.000Z"),
                ActivityType.RUNNING,
                10.5,
                8.2);
    }

    private static Training generateTrainingWithDetails(User user, String startTime, String endTime, ActivityType activityType, double distance, double averageSpeed) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        return new Training(
                user,
                sdf.parse(startTime),
                sdf.parse(endTime),
                activityType,
                distance,
                averageSpeed);
    }
}
