package com.capgemini.wsb.fitnesstracker.loader;

import com.capgemini.wsb.fitnesstracker.training.api.Training;
import com.capgemini.wsb.fitnesstracker.training.internal.ActivityType;
import com.capgemini.wsb.fitnesstracker.user.api.User;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.time.LocalDate.now;
import static java.util.Objects.isNull;

/**
 * Sample init data loader. If the application is run with `loadInitialData` profile, then on application startup it will fill the database with dummy data,
 * for the manual testing purposes. Loader is triggered by {@link ContextRefreshedEvent } event
 */
@Component
//@Profile("loadInitialData")
@Slf4j
@ToString
class InitialDataLoader {

    @Autowired
    private JpaRepository<User, Long> userRepository;

    @Autowired
    private JpaRepository<Training, Long> trainingRepository;

    @EventListener
    @Transactional
    @SuppressWarnings({"squid:S1854", "squid:S1481", "squid:S1192", "unused"})
    public void loadInitialData(ContextRefreshedEvent event) {
        verifyDependenciesAutowired();

        log.info("Loading initial data to the database");

        List<User> sampleUserList = generateSampleUsers();
        List<Training> sampleTrainingList = generateTrainingData(sampleUserList);

        log.info("Finished loading initial data");
    }

    private User generateUser(String name, String lastName, int age) {
        User user = new User(name,
                lastName,
                now().minusYears(age),
                "%s.%s@domain.com".formatted(name, lastName));
        return userRepository.save(user);
    }

    private List<User> generateSampleUsers() {
        List<User> users = new ArrayList<>();

        users.add(generateUser("Emma", "Johnson", 28));
        users.add(generateUser("Ethan", "Taylor", 51));
        users.add(generateUser("Olivia", "Davis", 76));
        users.add(generateUser("Daniel", "Thomas", 34));
        users.add(generateUser("Sophia", "Baker", 49));
        users.add(generateUser("Liam", "Jones", 23));
        users.add(generateUser("Ava", "Williams", 21));
        users.add(generateUser("Noah", "Miller", 39));
        users.add(generateUser("Grace", "Anderson", 33));
        users.add(generateUser("Oliver", "Swift", 29));

        return users;
    }

    private List<Training> generateTrainingData(List<User> users) {
        List<Training> trainingData = new ArrayList<>();

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date now = new Date();

            // Emma Johnson: 3 trainings
            trainingData.add(new Training(users.get(0), new Date(now.getTime() - 7 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 7 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.RUNNING, 10.5, 8.2));
            trainingData.add(new Training(users.get(0), new Date(now.getTime() - 6 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 6 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.CYCLING, 20.5, 15.0));
            trainingData.add(new Training(users.get(0), new Date(now.getTime() - 5 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 5 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.WALKING, 5.0, 4.0));

            // Ethan Taylor: 4 trainings
            trainingData.add(new Training(users.get(1), new Date(now.getTime() - 7 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 7 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.RUNNING, 15.5, 10.0));
            trainingData.add(new Training(users.get(1), new Date(now.getTime() - 6 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 6 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.CYCLING, 25.5, 20.0));
            trainingData.add(new Training(users.get(1), new Date(now.getTime() - 5 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 5 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.WALKING, 10.0, 5.0));
            trainingData.add(new Training(users.get(1), new Date(now.getTime() - 4 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 4 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.RUNNING, 12.0, 9.0));

            // Olivia Davis: 2 trainings
            trainingData.add(new Training(users.get(2), new Date(now.getTime() - 7 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 7 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.RUNNING, 12.5, 9.2));
            trainingData.add(new Training(users.get(2), new Date(now.getTime() - 6 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 6 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.CYCLING, 22.5, 17.0));

            // Daniel Thomas: 3 trainings
            trainingData.add(new Training(users.get(3), new Date(now.getTime() - 7 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 7 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.RUNNING, 11.5, 8.2));
            trainingData.add(new Training(users.get(3), new Date(now.getTime() - 6 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 6 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.CYCLING, 21.5, 16.0));
            trainingData.add(new Training(users.get(3), new Date(now.getTime() - 5 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 5 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.WALKING, 6.0, 4.5));

            // Sophia Baker: 2 trainings
            trainingData.add(new Training(users.get(4), new Date(now.getTime() - 7 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 7 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.RUNNING, 13.5, 9.8));
            trainingData.add(new Training(users.get(4), new Date(now.getTime() - 6 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 6 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.CYCLING, 23.5, 18.0));

            // Liam Jones: 1 training
            trainingData.add(new Training(users.get(5), new Date(now.getTime() - 7 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 7 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.WALKING, 7.0, 5.5));

            // Ava Williams: 4 trainings
            trainingData.add(new Training(users.get(6), new Date(now.getTime() - 7 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 7 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.RUNNING, 9.5, 7.2));
            trainingData.add(new Training(users.get(6), new Date(now.getTime() - 6 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 6 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.CYCLING, 19.5, 14.0));
            trainingData.add(new Training(users.get(6), new Date(now.getTime() - 5 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 5 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.WALKING, 8.0, 6.0));
            trainingData.add(new Training(users.get(6), new Date(now.getTime() - 4 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 4 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.RUNNING, 10.0, 8.0));

            // Noah Miller: 3 trainings
            trainingData.add(new Training(users.get(7), new Date(now.getTime() - 7 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 7 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.RUNNING, 14.5, 11.2));
            trainingData.add(new Training(users.get(7), new Date(now.getTime() - 6 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 6 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.CYCLING, 24.5, 19.0));
            trainingData.add(new Training(users.get(7), new Date(now.getTime() - 5 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 5 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.WALKING, 9.0, 7.0));

            // Grace Anderson: 2 trainings
            trainingData.add(new Training(users.get(8), new Date(now.getTime() - 7 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 7 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.RUNNING, 10.5, 8.2));
            trainingData.add(new Training(users.get(8), new Date(now.getTime() - 6 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 6 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.CYCLING, 20.5, 15.0));

            // Oliver Swift: 1 training
            trainingData.add(new Training(users.get(9), new Date(now.getTime() - 7 * 24 * 60 * 60 * 1000), new Date(now.getTime() - 7 * 24 * 60 * 60 * 1000 + 90 * 60 * 1000), ActivityType.WALKING, 5.0, 4.0));

            trainingData.forEach(training -> trainingRepository.save(training));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return trainingData;
    }

    private void verifyDependenciesAutowired() {
        if (isNull(userRepository)) {
            throw new IllegalStateException("Initial data loader was not autowired correctly " + this);
        }
    }
}
