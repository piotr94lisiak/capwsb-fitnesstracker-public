package com.capgemini.wsb.fitnesstracker.mail.internal;

import com.capgemini.wsb.fitnesstracker.mail.api.EmailDto;
import com.capgemini.wsb.fitnesstracker.mail.api.EmailSender;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class EmailTestController {

    private final EmailSender emailSender;

    @GetMapping("/test-email")
    public String sendTestEmail() {
        EmailDto emailDto = new EmailDto("test@test.com", "Test Email", "This is a test email.");
        emailSender.send(emailDto);
        return "Email sent";
    }
}
