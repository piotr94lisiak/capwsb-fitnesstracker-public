package com.capgemini.wsb.fitnesstracker.training.internal;

import com.capgemini.wsb.fitnesstracker.training.api.Training;
import com.capgemini.wsb.fitnesstracker.training.api.TrainingProvider;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of TrainingProvider that provides CRUD operations for Training.
 */
@Service
@RequiredArgsConstructor
@Slf4j
class TrainingServiceImpl implements TrainingProvider {

    private final TrainingRepository trainingRepository;

    @Override
    public Optional<Training> getTraining(Long trainingId) {
        return trainingRepository.findById(trainingId);
    }

    @Override
    public Training createTraining(Training training) {
        return trainingRepository.save(training);
    }

    @Override
    public Training updateTraining(Training training) {
        return trainingRepository.save(training);
    }

    @Override
    public void deleteTraining(Long id) {
        trainingRepository.deleteById(id);
    }

    @Override
    public List<Training> findFinishedTrainingsAfter(String afterTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate afterDate = LocalDate.parse(afterTime, formatter);
        LocalDateTime startOfDay = afterDate.atStartOfDay();
        return trainingRepository.findByEndTimeAfter(startOfDay);
    }

    @Override
    public List<Training> findByActivityType(String activityType) {
        return trainingRepository.findByActivityType(ActivityType.valueOf(activityType.toUpperCase()));
    }

    @Override
    public List<Training> findByUserId(Long userId) {
        log.info("Fetching trainings for user ID: {}", userId);
        List<Training> trainings = trainingRepository.findByUserId(userId);
        log.info("Found trainings: {}", trainings);
        return trainings;
    }

    @Override
    public List<Training> findAllTrainings() {
        return trainingRepository.findAll();
    }
}
