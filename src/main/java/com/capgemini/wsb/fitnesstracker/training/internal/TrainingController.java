package com.capgemini.wsb.fitnesstracker.training.internal;

import com.capgemini.wsb.fitnesstracker.training.api.Training;
import com.capgemini.wsb.fitnesstracker.training.api.TrainingDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * REST controller for managing trainings.
 */
@RestController
@RequestMapping("/v1/trainings")
@RequiredArgsConstructor
@Slf4j
public class TrainingController {

    private final TrainingServiceImpl trainingService;
    private final TrainingMapper trainingMapper;

    /**
     * GET  /v1/trainings : Get all trainings.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of trainings in body
     */
    @GetMapping
    public List<TrainingDto> getAllTrainings() {
        return trainingService.findAllTrainings()
                .stream()
                .map(trainingMapper::toDto)
                .collect(Collectors.toList());
    }

    /**
     * POST  /v1/trainings : Create a new training.
     *
     * @param trainingDto the trainingDto to create
     * @return the ResponseEntity with status 201 (Created) and with body the new trainingDto
     */
    @PostMapping
    public ResponseEntity<TrainingDto> createTraining(@RequestBody TrainingDto trainingDto) {
        Training training = trainingMapper.toEntity(trainingDto);
        Training savedTraining = trainingService.createTraining(training);
        return ResponseEntity.status(201).body(trainingMapper.toDto(savedTraining));
    }

    /**
     * PUT  /v1/trainings/{id} : Updates an existing training.
     *
     * @param id the id of the trainingDto to update
     * @param trainingDto the trainingDto to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated trainingDto
     */
    @PutMapping("/{id}")
    public ResponseEntity<TrainingDto> updateTraining(@PathVariable Long id, @RequestBody TrainingDto trainingDto) {
        Training training = trainingMapper.toEntity(trainingDto);
        training.setId(id); // Ensure the ID is set for update
        Training updatedTraining = trainingService.updateTraining(training);
        return ResponseEntity.ok(trainingMapper.toDto(updatedTraining));
    }

    /**
     * GET  /v1/trainings/{id} : Get the "id" training.
     *
     * @param id the id of the trainingDto to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the trainingDto, or with status 404 (Not Found)
     */
    @GetMapping("/{id}")
    public ResponseEntity<TrainingDto> getTrainingById(@PathVariable Long id) {
        return trainingService.getTraining(id)
                .map(trainingMapper::toDto)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    /**
     * GET  /v1/trainings/finished/{afterTime} : Get all finished trainings after the specified time.
     *
     * @param afterTime the date after which to retrieve finished trainings
     * @return the ResponseEntity with status 200 (OK) and the list of finished trainings in body
     */
    @GetMapping("/finished/{afterTime}")
    public ResponseEntity<List<TrainingDto>> getAllFinishedTrainingsAfterTime(@PathVariable String afterTime) {
        List<Training> trainings = trainingService.findFinishedTrainingsAfter(afterTime);
        List<TrainingDto> trainingDtos = trainings.stream()
                .map(trainingMapper::toDto)
                .collect(Collectors.toList());
        return ResponseEntity.ok(trainingDtos);
    }

    /**
     * GET  /v1/trainings/activityType : Get all trainings by activity type.
     *
     * @param activityType the type of activity to filter trainings by
     * @return the ResponseEntity with status 200 (OK) and the list of trainings in body
     */
    @GetMapping("/activityType")
    public ResponseEntity<List<TrainingDto>> getAllTrainingByActivityType(@RequestParam String activityType) {
        List<Training> trainings = trainingService.findByActivityType(activityType);
        List<TrainingDto> trainingDtos = trainings.stream()
                .map(trainingMapper::toDto)
                .collect(Collectors.toList());
        return ResponseEntity.ok(trainingDtos);
    }

    /**
     * GET  /v1/trainings/user/{userId} : Get all trainings for a specific user.
     *
     * @param userId the id of the user to retrieve trainings for
     * @return the ResponseEntity with status 200 (OK) and the list of trainings in body, or with status 404 (Not Found)
     */
    @GetMapping("/user/{userId}")
    public ResponseEntity<List<TrainingDto>> getAllTrainingsForDedicatedUser(@PathVariable Long userId) {
        List<Training> trainings = trainingService.findByUserId(userId);
        if (trainings.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        List<TrainingDto> trainingDtos = trainings.stream()
                .map(trainingMapper::toDto)
                .collect(Collectors.toList());
        return ResponseEntity.ok(trainingDtos);
    }

    /**
     * DELETE  /v1/trainings/{id} : Delete the "id" training.
     *
     * @param id the id of the training to delete
     * @return the ResponseEntity with status 204 (NO_CONTENT)
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTraining(@PathVariable Long id) {
        trainingService.deleteTraining(id);
        return ResponseEntity.noContent().build();
    }

    /**
     * GET  /v1/trainings/entities : Get all training entities.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of training entities in body
     */
    @GetMapping("/entities")
    public ResponseEntity<List<Training>> getAllTrainingEntities() {
        List<Training> trainings = trainingService.findAllTrainings();
        return ResponseEntity.ok(trainings);
    }
}
