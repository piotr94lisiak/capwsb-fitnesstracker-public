package com.capgemini.wsb.fitnesstracker.training.api;

import java.util.List;
import java.util.Optional;

public interface TrainingProvider {

    /**
     * Retrieves a training based on their ID.
     * If the training with the given ID is not found, then {@link Optional#empty()} will be returned.
     *
     * @param trainingId id of the training to be searched
     * @return An {@link Optional} containing the located Training, or {@link Optional#empty()} if not found
     */
    Optional<Training> getTraining(Long trainingId);

    /**
     * Creates a new training entry.
     *
     * @param training the training to create
     * @return the created training
     */
    Training createTraining(Training training);

    /**
     * Updates an existing training entry.
     *
     * @param training the training to update
     * @return the updated training
     */
    Training updateTraining(Training training);

    /**
     * Deletes a training entry by its ID.
     *
     * @param id the ID of the training to delete
     */
    void deleteTraining(Long id);

    /**
     * Retrieves all trainings finished after a specific time.
     *
     * @param afterTime the time after which trainings should be retrieved
     * @return a list of finished trainings
     */
    List<Training> findFinishedTrainingsAfter(String afterTime);

    /**
     * Retrieves all trainings by activity type.
     *
     * @param activityType the type of activity
     * @return a list of trainings with the specified activity type
     */
    List<Training> findByActivityType(String activityType);

    /**
     * Retrieves all trainings for a specific user.
     *
     * @param userId the ID of the user
     * @return a list of trainings for the user
     */
    List<Training> findByUserId(Long userId);

    /**
     * Retrieves all trainings.
     *
     * @return a list of all trainings
     */
    List<Training> findAllTrainings();
}
