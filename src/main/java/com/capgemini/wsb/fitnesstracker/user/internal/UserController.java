package com.capgemini.wsb.fitnesstracker.user.internal;

import com.capgemini.wsb.fitnesstracker.user.api.User;
import com.capgemini.wsb.fitnesstracker.user.api.UserDto;
import com.capgemini.wsb.fitnesstracker.user.api.UserSimpleDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * REST controller for managing {@link User} entities.
 */
@RestController
@RequestMapping("/v1/users")
@RequiredArgsConstructor
@Slf4j
class UserController {

    private final UserServiceImpl userService;
    private final UserMapper userMapper;

    /**
     * GET /v1/users/simple : Get all users with only basic information.
     *
     * @return the list of users
     */
    @GetMapping("/simple")
    public ResponseEntity<List<UserSimpleDto>> getAllSimpleUsers() {
        List<UserSimpleDto> users = userService.findAllUsers()
                .stream()
                .map(userMapper::toSimpleDto)
                .collect(Collectors.toList());
        return ResponseEntity.ok(users);
    }

    /**
     * GET /v1/users : Get all users.
     *
     * @return the list of users
     */
    @GetMapping
    public ResponseEntity<List<UserDto>> getAllUsers() {
        List<UserDto> users = userService.findAllUsers()
                .stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
        return ResponseEntity.ok(users);
    }

    /**
     * GET /v1/users/{id} : Get user by ID.
     *
     * @param id the ID of the user to retrieve
     * @return the user
     */
    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUserById(@PathVariable Long id) {
        return userService.getUser(id)
                .map(userMapper::toDto)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    /**
     * POST /v1/users : Create a new user.
     *
     * @param userDto the user to create
     * @return the created user
     */
    @PostMapping
    public ResponseEntity<UserDto> addUser(@RequestBody UserDto userDto) {
        User user = userMapper.toEntity(userDto);
        User savedUser = userService.createUser(user);
        return ResponseEntity.status(201).body(userMapper.toDto(savedUser));
    }

    /**
     * DELETE /v1/users/{id} : Delete a user by ID.
     *
     * @param id the ID of the user to delete
     * @return the response entity
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
        return ResponseEntity.noContent().build();
    }

    /**
     * PUT /v1/users/{id} : Update a user.
     *
     * @param id      the ID of the user to update
     * @param userDto the updated user
     * @return the updated user
     */
    @PutMapping("/{id}")
    public ResponseEntity<UserDto> updateUser(@PathVariable Long id, @RequestBody UserDto userDto) {
        User user = userMapper.toEntity(userDto);
        user.setId(id); // Ensure the ID is set for update
        User updatedUser = userService.updateUser(user);
        return ResponseEntity.ok(userMapper.toDto(updatedUser));
    }

    /**
     * GET /v1/users/email : Get user by email.
     *
     * @param email the email of the user to retrieve
     * @return the user
     */
    @GetMapping("/email")
    public ResponseEntity<UserDto> getUserByEmail(@RequestParam String email) {
        return userService.getUserByEmail(email)
                .map(userMapper::toDto)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    /**
     * GET /v1/users/search : Search users by email fragment.
     *
     * @param email the email fragment to search for
     * @return the list of users matching the email fragment
     */
    @GetMapping("/search")
    public ResponseEntity<List<UserDto>> searchUsersByEmail(@RequestParam String email) {
        List<UserDto> users = userService.searchUsersByEmail(email)
                .stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
        return ResponseEntity.ok(users);
    }

    /**
     * GET /v1/users/age : Get users older than a certain age.
     *
     * @param age the age threshold
     * @return the list of users older than the specified age
     */
    @GetMapping("/age")
    public ResponseEntity<List<UserDto>> searchUsersByAge(@RequestParam int age) {
        log.info("Getting users older than age: {}", age);
        List<UserDto> users = userService.findUsersOlderThan(age)
                .stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
        log.info("Found users: {}", users);
        return ResponseEntity.ok(users);
    }

    /**
     * GET /v1/users/older/{age} : Get users older than a certain age.
     *
     * @param age the age threshold
     * @return the list of users older than the specified age
     */
    @GetMapping("/older/{age}")
    public ResponseEntity<List<UserDto>> getUsersOlderThan(@PathVariable int age) {
        List<UserDto> users = userService.findUsersOlderThan(age)
                .stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
        return ResponseEntity.ok(users);
    }

    /**
     * GET /v1/users/firstname : Get users by first name.
     *
     * @param firstName the first name to search for
     * @return the list of users matching the first name
     */
    @GetMapping("/firstname")
    public ResponseEntity<List<UserDto>> getUsersByFirstName(@RequestParam String firstName) {
        List<UserDto> users = userService.findUsersByFirstName(firstName)
                .stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
        return ResponseEntity.ok(users);
    }

    /**
     * GET /v1/users/lastname : Get users by last name.
     *
     * @param lastName the last name to search for
     * @return the list of users matching the last name
     */
    @GetMapping("/lastname")
    public ResponseEntity<List<UserDto>> getUsersByLastName(@RequestParam String lastName) {
        List<UserDto> users = userService.findUsersByLastName(lastName)
                .stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
        return ResponseEntity.ok(users);
    }

    /**
     * GET /v1/users/birthdate : Get users by birthdate.
     *
     * @param birthdate the birthdate to search for
     * @return the list of users matching the birthdate
     */
    @GetMapping("/birthdate")
    public ResponseEntity<List<UserDto>> getUsersByBirthdate(@RequestParam LocalDate birthdate) {
        List<UserDto> users = userService.findUsersByBirthdate(birthdate)
                .stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
        return ResponseEntity.ok(users);
    }
}
