package com.capgemini.wsb.fitnesstracker.user.api;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Interface (API) for modifying operations on {@link User} entities through the API.
 * Implementing classes are responsible for executing changes within a database transaction,
 * whether by continuing an existing transaction or creating a new one if required.
 */
public interface UserService {

    /**
     * Creates a new user.
     *
     * @param user the user to create
     * @return the created user
     */
    User createUser(User user);

    /**
     * Deletes a user by ID.
     *
     * @param id the ID of the user to delete
     */
    void deleteUser(Long id);

    /**
     * Updates a user.
     *
     * @param user the user to update
     * @return the updated user
     */
    User updateUser(User user);

    /**
     * Searches for users by email fragment, case insensitive.
     *
     * @param email the email fragment to search for
     * @return the list of users matching the email fragment
     */
    List<User> searchUsersByEmail(String email);

    /**
     * Finds users older than a specified age.
     *
     * @param age the age threshold
     * @return the list of users older than the specified age
     */
    List<User> findUsersOlderThan(int age);

    /**
     * Retrieves a user by ID.
     *
     * @param userId the ID of the user to retrieve
     * @return the optional user, or empty if not found
     */
    Optional<User> getUser(Long userId);

    /**
     * Retrieves a user by email.
     *
     * @param email the email of the user to retrieve
     * @return the optional user, or empty if not found
     */
    Optional<User> getUserByEmail(String email);

    /**
     * Retrieves all users.
     *
     * @return the list of all users
     */
    List<User> findAllUsers();

    /**
     * Finds users by first name, case insensitive.
     *
     * @param firstName the first name to search for
     * @return the list of users matching the first name
     */
    List<User> findUsersByFirstName(String firstName);

    /**
     * Finds users by last name, case insensitive.
     *
     * @param lastName the last name to search for
     * @return the list of users matching the last name
     */
    List<User> findUsersByLastName(String lastName);

    /**
     * Finds users by birthdate.
     *
     * @param birthdate the birthdate to search for
     * @return the list of users matching the birthdate
     */
    List<User> findUsersByBirthdate(LocalDate birthdate);
}
