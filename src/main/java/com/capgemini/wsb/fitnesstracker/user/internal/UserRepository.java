package com.capgemini.wsb.fitnesstracker.user.internal;

import com.capgemini.wsb.fitnesstracker.user.api.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Query searching users by email address. It matches by exact match.
     *
     * @param email email of the user to search
     * @return {@link Optional} containing found user or {@link Optional#empty()} if none matched
     */
    default Optional<User> findByEmail(String email) {
        return findAll().stream()
                .filter(user -> Objects.equals(user.getEmail(), email))
                .findFirst();
    }

    /**
     * Query searching users by email address containing a fragment, case insensitive.
     *
     * @param email fragment of the email of the user to search
     * @return List of {@link User} containing found users
     */
    List<User> findByEmailContainingIgnoreCase(String email);

    /**
     * Query searching users born before a certain date.
     *
     * @param date birthdate cutoff
     * @return List of {@link User} containing found users
     */
    List<User> findByBirthdateBefore(LocalDate date);

    /**
     * Query searching users by first name.
     *
     * @param firstName first name of the user to search
     * @return List of {@link User} containing found users
     */
    List<User> findByFirstName(String firstName);

    /**
     * Query searching users by last name.
     *
     * @param lastName last name of the user to search
     * @return List of {@link User} containing found users
     */
    List<User> findByLastName(String lastName);

    /**
     * Query searching users by birthdate.
     *
     * @param birthdate birthdate of the user to search
     * @return List of {@link User} containing found users
     */
    List<User> findByBirthdate(LocalDate birthdate);
}
