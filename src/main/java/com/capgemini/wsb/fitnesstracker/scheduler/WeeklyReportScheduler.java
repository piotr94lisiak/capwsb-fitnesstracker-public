package com.capgemini.wsb.fitnesstracker.scheduler;

import com.capgemini.wsb.fitnesstracker.mail.api.EmailDto;
import com.capgemini.wsb.fitnesstracker.mail.api.EmailSender;
import com.capgemini.wsb.fitnesstracker.training.api.Training;
import com.capgemini.wsb.fitnesstracker.training.internal.TrainingRepository;
import com.capgemini.wsb.fitnesstracker.user.api.User;
import com.capgemini.wsb.fitnesstracker.user.internal.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
public class WeeklyReportScheduler {

    private final UserRepository userRepository;
    private final TrainingRepository trainingRepository;
    private final EmailSender emailSender;

    @Scheduled(fixedRate = 5000)
    public void generateAndSendWeeklyReports() {
        log.info("Starting report generation...");

        LocalDateTime startOfWeek = LocalDate.now().minusWeeks(1).atStartOfDay();
        LocalDateTime endOfWeek = LocalDate.now().atTime(LocalTime.MAX);

        List<User> users = userRepository.findAll();
        log.info("Found {} users", users.size());
        for (User user : users) {
            List<Training> userTrainings = trainingRepository.findByUserId(user.getId()).stream()
                    .filter(training -> {
                        LocalDateTime trainingEndTime = training.getEndTime().toInstant()
                                .atZone(ZoneId.systemDefault()).toLocalDateTime();
                        return trainingEndTime.isAfter(startOfWeek) && trainingEndTime.isBefore(endOfWeek);
                    })
                    .collect(Collectors.toList());

            if (!userTrainings.isEmpty()) {
                String reportContent = generateReportContent(user, userTrainings);
                EmailDto email = new EmailDto(user.getEmail(), "Weekly Training Report", reportContent);
                emailSender.send(email);
                log.info("Report sent to: {}", user.getEmail());
            } else {
                log.info("No trainings found for user: {}", user.getEmail());
            }
        }
        log.info("Report generation completed.");
    }

    private String generateReportContent(User user, List<Training> trainings) {
        StringBuilder content = new StringBuilder();
        content.append("Weekly Training Report for ").append(user.getFirstName()).append(" ").append(user.getLastName()).append("\n");
        content.append("Total trainings: ").append(trainings.size()).append("\n\n");

        for (Training training : trainings) {
            content.append("Training ID: ").append(training.getId()).append("\n");
            content.append("Activity Type: ").append(training.getActivityType()).append("\n");
            content.append("Start Time: ").append(training.getStartTime()).append("\n");
            content.append("End Time: ").append(training.getEndTime()).append("\n");
            content.append("Distance: ").append(training.getDistance()).append(" km\n");
            content.append("Average Speed: ").append(training.getAverageSpeed()).append(" km/h\n");
            content.append("\n");
        }

        return content.toString();
    }
}
